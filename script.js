var num = 2
var getCube = num ** 3  
console.log(`The cube of ${num} is ${getCube}`)

const address = [`Blk 10 Lot 14`, `Eon Centennial Homes`,`Hibao An, Pavia`,`Iloilo City`]
const [blkNo,sub,mun,prov] = address;
console.log(`I live at ${blkNo},${sub},${mun},${prov}`)

const animal = {
	animalName: `mimi`,
	animalSpecies: `cat`,
	animalWeight: 15
}
const {animalName,animalSpecies,animalWeight} = animal;
console.log(`${animalName} is a ${animalSpecies} that weights ${animalWeight} lbs.`)

const numbers = [5,10,15,20]
numbers.forEach((numP) => console.log(numP))

class Dog{
	constructor(name,age,breed){
		this.name = name,
		this.age = age,
		this.breed = breed 
	}
}

const newDog = new Dog(`Tammy`, `5`, `Japanese Spits`)
console.log(newDog)